alias cal='cal -A1 -B1'
alias dig9='dig @9.9.9.9 +noall +answer'
alias digi='dig +noall +answer'
alias gitdifflast='git diff HEAD^ HEAD'
alias gitlog='git log --oneline --decorate --graph --color -22'
alias gitstatus='git status -sb'
alias startx='startx > /dev/null 2>&1'
