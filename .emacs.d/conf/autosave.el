;; stop creating backup~ files
(setq make-backup-files nil)

;; stop creating #autosave# files
(setq auto-save-default nil)

;; stop creating #lock files
(setq create-lockfiles nil)

