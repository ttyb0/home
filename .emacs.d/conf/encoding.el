;; encoding
(prefer-coding-system 'utf-8-unix)
(setq-default buffer-file-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'utf-8-unix)

