;; no tabs
(setq-default indent-tabs-mode nil)

;; fill column (M-q / C-u M-q)
(setq-default fill-column 80)
