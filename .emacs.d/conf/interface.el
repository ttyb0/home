;; window mode
(if window-system (menu-bar-mode -1))
(if window-system (tool-bar-mode -1))
(if window-system (scroll-bar-mode -1))

;; number mode
(line-number-mode)
(column-number-mode)

