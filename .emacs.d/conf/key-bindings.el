;; key bindings
(global-set-key (kbd "M-t") 'backward-char)
(global-set-key (kbd "M-s") 'previous-line)
(global-set-key (kbd "M-r") 'next-line)
(global-set-key (kbd "M-n") 'forward-char)

(global-set-key (kbd "M-a") 'move-beginning-of-line)
(global-set-key (kbd "M-u") 'backward-paragraph)
(global-set-key (kbd "M-i") 'forward-paragraph)
(global-set-key (kbd "M-e") 'move-end-of-line)

;; kill buffer and window
(global-set-key (kbd "C-x C-k") 'kill-buffer-and-window)

;; auto completion
(global-set-key [C-tab] 'dabbrev-expand)

;; revert buffer
(global-set-key [f5] 'revert-buffer)

;; flyspell-prog-mode
(global-set-key [f6] 'flyspell-prog-mode)

;; flyspell-mode
(global-set-key [f7] 'flyspell-mode)

;; ztree
(global-set-key [f8] 'ztree-dir)

;; whitespace-mode
(global-set-key [f9] 'whitespace-mode)
