;; startup message
(setq inhibit-startup-message t)

;; scratch mode
(setq initial-major-mode 'markdown-mode)

;; scratch message
(setq initial-scratch-message
      (insert-file-contents "~/.emacs.d/scratch-message"))
