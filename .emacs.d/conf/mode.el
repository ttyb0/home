;; mode dir
(add-to-list 'load-path "~/.emacs.d/mode/")

;; powershell mode
(load "powershell-mode.el")
(add-to-list 'auto-mode-alist '("\\.psd?m?1\\'" . powershell-mode))
