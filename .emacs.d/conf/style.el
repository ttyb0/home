;; font
(set-face-font 'default "DejaVu Sans Mono")
(set-face-attribute 'default nil :height 100)

;; text colors
(if window-system (set-foreground-color "#EEE"))
(if window-system (set-background-color "#333"))
(if window-system (set-face-foreground 'font-lock-string-face "#8F8"))
(if window-system (set-face-foreground 'font-lock-comment-face "#888"))
(if window-system (set-face-foreground 'font-lock-keyword-face "#4FF"))
(if window-system (set-face-foreground 'font-lock-builtin-face "#8CF"))
(if window-system (set-face-foreground 'font-lock-variable-name-face "#FF8"))
(if window-system (set-face-attribute 'region nil :background "#48F" :foreground "#EEE"))

;; mode-line
(if window-system (set-face-attribute 'mode-line nil :box nil))
(if window-system (set-face-attribute 'mode-line-inactive nil :box nil))
(if window-system (set-face-foreground 'mode-line "#EEE"))
(if window-system (set-face-background 'mode-line "#111"))
(if window-system (set-face-foreground 'mode-line-inactive "#888"))
(if window-system (set-face-background 'mode-line-inactive "#111"))

;; fringe
(if window-system (set-face-foreground 'fringe "#F88"))
(if window-system (set-face-background 'fringe "#111"))

;; vertical-border (divider)
(if window-system (set-face-foreground 'vertical-border "#111"))
