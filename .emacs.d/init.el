;; init emacs conf

;; conf dir
(add-to-list 'load-path "~/.emacs.d/conf/")

;; load conf
(load "interface.el")
(load "style.el")
(load "message.el")
(load "key-bindings.el")
(load "encoding.el")
(load "indent.el")
(load "autosave.el")
(load "ztree.el")
(load "mode.el")

;; local conf
(load "local.el" t)
