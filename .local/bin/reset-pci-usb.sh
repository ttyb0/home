#!/usr/bin/env sh

# check if user is root
if [ $(id -u) -ne 0 ]
then
    echo "\n root account required !\n"
    exit 1
fi

echo # newline

# pci usb list
lspci | grep USB
pci_usb_list=$(lspci | grep USB | cut -d ' ' -f 1)

echo # newline

# set Internal Field Separator
IFS='
'

# unbind all pci usb
for pci_usb in $pci_usb_list
do
    echo "unbind $pci_usb"
    echo -n "0000:${pci_usb}" > /sys/bus/pci/drivers/xhci_hcd/unbind
done

echo # newline

# bind all pci usb
for pci_usb in $pci_usb_list
do
    echo "bind $pci_usb"
    echo -n "0000:${pci_usb}" > /sys/bus/pci/drivers/xhci_hcd/bind
done

echo # newline
