#!/bin/sh
#===============================================================================
#|
#|skeleton.sh
#|
#| DESCRIPTION
#|
#|   Script modèle.
#|
#| USAGE
#|
#|   skeleton.sh [options]
#|
#| PARAMÈTRES
#|
#|   Pas de paramètres.
#|
#| OPTIONS
#|
#|   -h : Affiche cette aide.
#|   -l : Écrit tous les messages dans un fichier.
#|   -q : Désactive l'affichage de tous les messages.
#|   -v : Active l'affichage des messages d'information.
#|
#===============================================================================

#===============================================================================
# Paramètres
#===============================================================================

# Nombre de paramètres
PAR_NB=0

#===============================================================================
# Options
#===============================================================================

# initialisation des options avec une valeur par défaut
OPT_LOG=false
OPT_QUIET=false
OPT_VERBOSE=false

#===============================================================================
# Variables
#===============================================================================

# séparateur de nouvelle ligne
NLS="
"

# fichier de log
LOG_DIR="$(dirname ${0})"
LOG_NAME="$(basename ${0})_$(date +'%Y%m%d%H%M%S').log"
LOG_FILE="${LOG_DIR}/${LOG_NAME}"

# codes exit
EC_OK=0  # Normal.
EC_OPT=1 # Erreur lors de la vérification des options.
EC_PAR=2 # Erreur lors de la vérification des paramètres.

# initialisation du code d'état
STATUS_CODE=$EC_OK

#===============================================================================
# Fonctions
#===============================================================================

# Fonction d'affichage de l'aide
usage() {
    grep -e '^#|' $0 | sed 's/#|//'
}

# Fonction de récupération des arguments
getArgs() {

    # récupération des options
    # OPT_X=$OPTARG
    while getopts 'hlqv' opt
    do
        case $opt in
            h) STATUS_CODE=$EC_OPT ;;
            l) OPT_LOG=true ;;
            q) OPT_QUIET=true ;;
            v) OPT_VERBOSE=true ;;
            \?) STATUS_CODE=$EC_OPT ;;
        esac
    done

    # récupération des paramètres
    # PAR_1=$1, PAR_2=$2, …
    shift $(($OPTIND - 1))
    if [ $# -eq $PAR_NB ]
    then
        PAR_SCRIPT=$(basename $0)
    else
        STATUS_CODE=$EC_PAR
    fi

    # affichage de l'aide en cas d'erreur
    test $STATUS_CODE -ne $EC_OK && usage
}

# Fonction de vérification l'état d'une commande
# $1 : code retour à vérifier
# $2 : code à retourner en cas d'erreur
# $3 : message
checkStatus() {
    if [ "$1" -eq $EC_OK ]
    then
        msg inf "${3} : ok."
    else
        msg err "${3} : KO !"
        STATUS_CODE=$2
    fi
    return $STATUS_CODE
}

# Fonction d'affichage des messages
# $1 : niveau du message (inf, war, err)
# $2 : message
msg() {
    # horodatage
    msg_hd=$(date +'%Y-%m-%d %H:%M:%S')

    # niveau du message
    case $1 in
        inf) msg_lvl="inf" ;;
        war) msg_lvl="War" ;;
        err) msg_lvl="ERR" ;;
        *)   msg_lvl="???" ;;
    esac

    # gestion du message
    IFS=$NLS
    for msg_line in $2
    do
        # construction du message
        msg_full="${msg_hd} ${PAR_SCRIPT} ${msg_lvl} ${msg_line}"

        # affichage du message
        case $1 in
            inf) [ $OPT_QUIET = false ] && [ $OPT_VERBOSE = true ] \
                     && printf '%s\n' "$msg_full" ;;
            war) [ $OPT_QUIET = false ] \
                     && printf '%s\n' "$msg_full" ;;
            err) printf '%s\n' "$msg_full" ;;
            *)   printf '%s\n' "$msg_full" ;;
        esac

        # impression du message
        [ $OPT_LOG = true ] && printf '%s\n' "$msg_full" >> $LOG_FILE
    done

    # unset
    unset msg_hd msg_lvl msg_line msg_full
}

#===============================================================================
# Traitement
#===============================================================================

main() {

    # récupération des arguments
    getArgs "$@"

    # Traitement
    if [ $STATUS_CODE -eq 0 ]
    then
        # Début du script
        msg inf "Début du script…"

        msg war "script en développement !"

        # Fin du script
        checkStatus $STATUS_CODE $STATUS_CODE "Fin du script"
    fi

    # exit
    exit $STATUS_CODE
}

#===============================================================================
# Exécution
#===============================================================================

main "$@"
