#!/bin/sh
#===============================================================================
# dotfiles install
#===============================================================================

# git directory with the dotfiles
GIT_DIR=$(cd $(dirname "$0") && pwd)

# files list in git directory
GIT_FILE_LIST=$(find "$GIT_DIR" \
                     -type f \
                     ! -path "*/\.git/*" \
                     ! -name "dotfiles-install.sh" \
             )

# browsing git files
for git_file in $GIT_FILE_LIST
do
    # absolute path of home file
    home_file=$(echo $git_file | sed "s_${GIT_DIR}_${HOME}_g")
    echo
    if [ -e "$home_file" ]
    then
        rm -v $home_file
        ln -vs $git_file $home_file
    else
        mkdir -vp $(dirname $home_file)
        ln -vs $git_file $home_file
    fi
done

# specific fix
echo
test -e ${HOME}/.config/redshift.conf && rm -v ${HOME}/.config/redshift.conf
ln -v ${GIT_DIR}/.config/redshift.conf ${HOME}/.config/redshift.conf
